'use strict';

var app = angular.module('testApp');
app.controller('MasterController', ['$scope', 'InsuranceApiService',
    function($scope, InsuranceApiService) {
        $scope.params = { vehicleMake: "", policyType: "" };
        $scope.paramsValidations = { vehicleMake: false, policyType: false };
        $scope.saveVehicleMakeMessage = false;
        $scope.savePolicyTypeMessage = false;
        $scope.addNewMake = function() {
            if ($scope.params.vehicleMake) {
                $scope.paramsValidations.vehicleMake = false;
                InsuranceApiService.addNewMake($scope.params.vehicleMake)
                    .then(function(response) {
                        $scope.saveVehicleMakeMessage = true;
                        $scope.params.vehicleMake = "";
                    }, function(error) {
                        console.log("Error in saving vehicle make", error);
                    });

            } else {
                $scope.paramsValidations.vehicleMake = true;
            }
        };


        $scope.addNewPolicyType = function() {
            if ($scope.params.policyType) {
                $scope.paramsValidations.policyType = false;
                InsuranceApiService.addPolicyTYpe($scope.params.policyType)
                    .then(function(response) {
                        $scope.savePolicyTypeMessage = true;
                        $scope.params.policyType = "";
                    }, function(error) {
                        console.log("Error in saving vehicle make", error);
                    });

            } else {
                $scope.paramsValidations.policyType = true;
            }
        }
    }

]);