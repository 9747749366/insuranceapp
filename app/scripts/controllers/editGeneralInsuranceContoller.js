app.controller('EditGeneralInsuranceCntrl', function($scope, $uibModalInstance, generalInsObj, policyTypeList, InsuranceApiService, $filter) {
    $scope.policyTypeList = policyTypeList;

    $scope.generalInsuranceParams = {
        entryDate: new Date(generalInsObj.EntryDate),
        name: generalInsObj.Name,
        address: generalInsObj.Address,
        email: generalInsObj.Email,
        premiumAmount: generalInsObj.PremiumAmount,
        phoneNumber: generalInsObj.PhoneNumber,
        policyNumber: generalInsObj.PolicyNumber,
        sumAssured: generalInsObj.SumAssured,
        expiryDate: new Date(generalInsObj.ExpiryDate),
        mobileNumber: [{ name: generalInsObj.MobileNumber1 }],
        generalInsuranceId: generalInsObj.GeneralInsuranceId,
        policyType: generalInsObj.PolicyType
    };


    if (generalInsObj.MobileNumber2) {
        $scope.generalInsuranceParams.mobileNumber.push({ name: generalInsObj.MobileNumber2 })
    };

    if (generalInsObj.MobileNumber3) {
        $scope.generalInsuranceParams.mobileNumber.push({ name: generalInsObj.MobileNumber3 })
    };

    $scope.saveGeneralInsMessage = false;

    $scope.editGeneralInsDisabled = false;
    $scope.dateFormat = 'dd/MM/yyyy';

    $scope.expiryDatePopup = {
        opened: false
    };
    $scope.entryDatePopup = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };
    $scope.openExpiryDate = function() {
        $scope.expiryDatePopup.opened = !$scope.expiryDatePopup.opened;
    };
    $scope.openEntryDate = function() {
        $scope.entryDatePopup.opened = !$scope.entryDatePopup.opened;
    };
    $scope.cancel = function() {
        $uibModalInstance.close('ok');

    };

    $scope.addNewNumber = function(idx) {
        var numList = $scope.generalInsuranceParams.mobileNumber;
        if (numList[idx].name) {
            $scope.generalInsuranceParams.mobileNumber.push({ name: "" });
        }

    };

    $scope.showNumPlus = function(idx) {
        var numList = $scope.generalInsuranceParams.mobileNumber;
        if (numList.length - 1 == idx) {
            return true;
        }
    };



    $scope.deleteNewNumber = function(idx) {
        $scope.generalInsuranceParams.mobileNumber.splice(idx, 1);
    };



    $scope.saveGeneralInsDetails = function(form) {
        if (form.$valid) {
            $scope.editGeneralInsDisabled = true;
            var generalInsParams = $scope.generalInsuranceParams;
            InsuranceApiService.editGeneralInsurance(generalInsParams, $scope.generalInsuranceParams.generalInsuranceId)
                .then(function(response) {
                    $scope.saveGeneralInsMessage = true;
                    $scope.editGeneralInsDisabled = false;

                }, function(error) {
                    console.log("Error in saving general inusrance", error);
                });
        }

    };

});