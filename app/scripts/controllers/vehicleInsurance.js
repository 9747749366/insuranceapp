'use strict';

var app = angular.module('testApp');
app.controller('VehicleInsuranceCntrl', ['$scope', 'InsuranceApiService', '$uibModal', '$filter', '$window', '$mdDateLocale',
    function($scope, InsuranceApiService, $uibModal, $filter, $window, $mdDateLocale) {


        $scope.vehicleinsuranceParams = {
            entryDate: new Date(),
            mobileNumber: [{ name: "" }],
            rcOwner: "",
            address: "",
            vehicleNumber: "",
            policyNumber: "",
            email: "",
            premiumAmount: "",
            presentOwner: "",
            address: "",
            expiryDate: new Date().setFullYear(new Date().getFullYear() + 1),
            make: {},
            phoneNumber: ""
        }

        $scope.vehicleInsSearchResults = [];

        $scope.vehicleInsSearchParams = { vehicleNumber: "", policyNumber: "", mobileNumber: "" };

        $scope.saveVehicleInsMessage = false;

        $scope.vehicleInsObj = {};

        $scope.makeList = [];

        $scope.policyTypeList = [];

        $scope.saveVehInsButtonDisabled = false;

        $scope.dateFormat = 'dd/MM/yyyy';

        $scope.expiryDatePopup = {
            opened: false
        };
        $scope.entryDatePopup = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: 'false'
        };
        $scope.openExpiryDate = function() {
            $scope.expiryDatePopup.opened = !$scope.expiryDatePopup.opened;
        };
        $scope.openEntryDate = function() {
            $scope.entryDatePopup.opened = !$scope.entryDatePopup.opened;
        };
        $scope.addNewNumber = function(idx) {
            var numList = $scope.vehicleinsuranceParams.mobileNumber;
            if (numList[idx].name) {
                $scope.vehicleinsuranceParams.mobileNumber.push({ name: "" });
            }

        };
        $scope.sameAsPresentOwner = function() {
            if ($scope.vehicleinsuranceParams.sameAs) {
                $scope.vehicleinsuranceParams.presentOwner = $scope.vehicleinsuranceParams.rcOwner
            } else {
                $scope.vehicleinsuranceParams.presentOwner = "";
            }
        };
        $scope.showNumPlus = function(idx) {
            var numList = $scope.vehicleinsuranceParams.mobileNumber;
            if (numList.length - 1 == idx) {
                return true;
            }
        };

        $scope.saveVehicleInsDetails = function(form) {
            if (form.$valid) {
                $scope.saveVehInsButtonDisabled = true;
                var vehicleInsParams = $scope.vehicleinsuranceParams;
                InsuranceApiService.saveVehicleInsurance(vehicleInsParams, 0)
                    .then(function(response) {
                        $scope.vehicleSaveResponseReceived();
                    }, function(error) {
                        console.log("Error in saving general inusrance", error);
                    });
            }
        };

        $scope.vehicleSaveResponseReceived = function() {
            $window.scrollTo(0, 0);
            $scope.saveVehicleInsMessage = true;
            $scope.saveVehInsButtonDisabled = false;
            /* $scope.vehicleinsuranceParams = {
                 entryDate: $filter('date')(new Date(), "dd/MM/yyyy"),
                 mobileNumber: [{ name: "" }],
                 rcOwner: "",
                 address: "",
                 vehicleNumber: "",
                 policyNumber: "",
                 email: "",
                 premiumAmount: "",
                 presentOwner: "",
                 address: "",
                 expiryDate: moment(new Date().setFullYear(new Date().getFullYear() + 1)),
                 make: {},
                 phoneNumber: ""
             }*/


        };

        $scope.deleteNewNumber = function(idx) {
            $scope.vehicleinsuranceParams.mobileNumber.splice(idx, 1);
        };
        $scope.addNewMake = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/views/AddMake.html',
                controller: 'AddMakeCntrl',
                size: 'sm',
                windowClass: '',
                resolve: {

                }
            });

            modalInstance.result.then(function(selectedItem) {
                InsuranceApiService.getMake()
                    .then(function(response) {
                        $scope.makeList = response.data;
                    }, function(error) {
                        console.log("error", error)
                    });
            });
        };

        $scope.showSaveVehileInsForm = function() {
            $scope.vehicleInsSearchResults = [];
            $scope.saveVehicleInsMessage = false;
            $scope.vehicleinsuranceParams = {
                entryDate: new Date(),
                mobileNumber: [{ name: "" }],
                rcOwner: "",
                address: "",
                vehicleNumber: "",
                policyNumber: "",
                email: "",
                premiumAmount: "",
                presentOwner: "",
                address: "",
                expiryDate: new Date().setFullYear(new Date().getFullYear() + 1),
                make: {},
                phoneNumber: ""
            }
        };

        $scope.showEditVehicleInsModal = function(idx) {
            $scope.vehicleInsObj = $scope.vehicleInsSearchResults[idx];
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/views/EditVehicleInsurance.html',
                controller: 'EditVehicleInsuranceCntrl',
                size: 'lg',
                windowClass: '',
                resolve: {
                    vehicleInsObj: function() {
                        return $scope.vehicleInsObj;
                    },
                    makeList: function() {
                        return $scope.makeList;
                    }

                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.vehInsApiSearch();
            });

        };

        $scope.showRenewVehicleInsModal = function(idx) {
            $scope.vehicleInsObj = $scope.vehicleInsSearchResults[idx];
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/views/RenewVehicleInsurance.html',
                controller: 'RenewVehicleInsuranceCntrl',
                size: 'lg',
                windowClass: '',
                resolve: {
                    vehicleInsObj: function() {
                        return $scope.vehicleInsObj;
                    },
                    makeList: function() {
                        return $scope.makeList;
                    }

                }
            });
            modalInstance.result.then(function(selectedItem) {
                $scope.vehInsApiSearch();
            });
        };


        $scope.searchVehcileIns = function(event) {
            if (event.keyCode == 13) {
                $scope.vehInsApiSearch();
            }
        };

        $scope.vehInsSearch = function() {
            $scope.vehInsApiSearch();
        };
        $scope.vehInsApiSearch = function() {
            var params = $scope.vehicleInsSearchParams;
            InsuranceApiService.searchVehicleIns(params, 0)
                .then(function(response) {
                    $scope.vehicleInsSearchResults = response.data;
                }, function(error) {
                    console.log("Error in search general inusrance", error);
                });
        };
        $scope.loadApiRequests = function() {
            InsuranceApiService.getMake()
                .then(function(response) {
                    $scope.makeList = response.data;
                }, function(error) {
                    console.log("error", error)
                });

        };


    }
]);