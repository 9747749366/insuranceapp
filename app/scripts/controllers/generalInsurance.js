'use strict';



var app = angular.module('testApp');
app.controller('GeneralInsuranceCntrl', ['$scope', 'InsuranceApiService', '$uibModal', '$filter', '$window', '$mdDateLocale',
    function($scope, InsuranceApiService, $uibModal, $filter, $window, $mdDateLocale) {

        $scope.generalInsuranceParams = {
            entryDate: new Date(),
            mobileNumber: [{ name: "" }],
            name: "",
            address: "",
            email: "",
            premiumAmount: "",
            sumAssured: "",
            expiryDate: new Date().setFullYear(new Date().getFullYear() + 1),
            policyType: {}
        };


        $scope.dateFormat = 'dd/MM/yyyy';

        $scope.expiryDatePopup = {
            opened: false
        };
        $scope.entryDatePopup = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: 'false'
        };
        $scope.openExpiryDate = function() {
            $scope.expiryDatePopup.opened = !$scope.expiryDatePopup.opened;
        };
        $scope.openEntryDate = function() {
            $scope.entryDatePopup.opened = !$scope.entryDatePopup.opened;
        };

        $scope.generalInsSearchParams = { MobileNumber: "", PolicyNumber: "" };

        $scope.generalInsSearchResults = [];

        $scope.saveGeneralInsMessage = false;

        $scope.generalInsObj = {};

        $scope.saveGenInsButtonDisabled = false;

        $scope.policyTypeList = [];

        $scope.generalInsPolictType = function() {
            InsuranceApiService.getPolicyType()
                .then(function(response) {
                    $scope.policyTypeList = response.data;
                }, function(error) {
                    console.log("error", error)
                });
        };


        $scope.addNewNumber = function(idx) {
            var numList = $scope.generalInsuranceParams.mobileNumber;
            if (numList[idx].name) {
                $scope.generalInsuranceParams.mobileNumber.push({ name: "" });
            }

        };

        $scope.showNumPlus = function(idx) {
            var numList = $scope.generalInsuranceParams.mobileNumber;
            if (numList.length - 1 == idx) {
                return true;
            }
        };

        $scope.saveGeneralInsDetails = function(form) {
            if (form.$valid) {
                $scope.saveGenInsButtonDisabled = true;
                var generalInsParams = $scope.generalInsuranceParams;
                InsuranceApiService.saveGeneralInsuranceDetails(generalInsParams, 0)
                    .then(function(response) {
                        $scope.generalInsSubmitResReceived();
                    }, function(error) {
                        console.log("Error in saving general inusrance", error);
                    });
            }

        };

        $scope.generalInsSubmitResReceived = function() {
            $window.scrollTo(0, 0);
            $scope.saveGeneralInsMessage = true;
            $scope.saveGenInsButtonDisabled = false;
           /*  $scope.generalInsuranceParams = {
                entryDate: $filter('date')(new Date(), "dd/MM/yyyy"),
                mobileNumber: [{ name: "" }],
                name: "",
                address: "",
                email: "",
                premiumAmount: "",
                sumAssured: "",
                expiryDate: $filter('date')(new Date().setFullYear(new Date().getFullYear() + 1), "dd/MM/yyyy"),
                policyType: {}
            };*/
        };

        $scope.deleteNewNumber = function(idx) {
            $scope.generalInsuranceParams.mobileNumber.splice(idx, 1);
        };

        $scope.searchGenIns = function() {
            $scope.generalInsuranceApiSearch();
        };
        
        $scope.showSaveGeneralInsForm = function() {
            $scope.generalInsSearchResults = [];
            $scope.saveGeneralInsMessage = false;
            $scope.generalInsuranceParams = {
                entryDate: $filter('date')(new Date(), "dd/MM/yyyy"),
                mobileNumber: [{ name: "" }],
                name: "",
                address: "",
                email: "",
                premiumAmount: "",
                sumAssured: "",
                expiryDate: $filter('date')(new Date().setFullYear(new Date().getFullYear() + 1), "dd/MM/yyyy"),
                policyType: {}
            };
        };

        $scope.searchGeneralIns = function(event) {
            if (event.keyCode == 13) {
                $scope.generalInsuranceApiSearch();

            }
        };
        $scope.generalInsuranceApiSearch = function() {
            var params = $scope.generalInsSearchParams;
            InsuranceApiService.searchGeneralIns(params, 0)
                .then(function(response) {
                    $scope.generalInsSearchResults = response.data;
                }, function(error) {
                    console.log("Error in search general inusrance", error);
                });
        };
        $scope.showEditGeneralInsModal = function(idx) {
            $scope.generalInsObj = $scope.generalInsSearchResults[idx];
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/views/EditGeneralInsrunace.html',
                controller: 'EditGeneralInsuranceCntrl',
                size: 'lg',
                windowClass: '',
                resolve: {
                    generalInsObj: function() {
                        return $scope.generalInsObj;
                    },
                    policyTypeList: function() {
                        return $scope.policyTypeList;
                    }

                }
            });


            modalInstance.result.then(function(selectedItem) {
                $scope.generalInsuranceApiSearch();
            });

        };

        $scope.showRenewGeneralInsModal = function(idx) {
            $scope.generalInsObj = $scope.generalInsSearchResults[idx];
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/views/RenewGeneralInsurance.html',
                controller: 'RenewGeneralInsuranceCntrl',
                size: 'lg',
                windowClass: '',
                resolve: {
                    generalInsObj: function() {
                        return $scope.generalInsObj;
                    },
                    policyTypeList: function() {
                        return $scope.policyTypeList;
                    }


                }
            });

           /* modalInstance.result.then(function(selectedItem) {
                $scope.generalInsSearchResults = [];
            });*/
        };






    }
]);