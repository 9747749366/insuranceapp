'use strict';

var app = angular.module('testApp');
app.controller('GeneralInsReportController', ['$scope', 'InsuranceApiService', '$uibModal', '$filter', 'uiGridExporterService', 'uiGridExporterConstants', 'ReportApiService', '$mdDateLocale',
    function($scope, InsuranceApiService, $uibModal, $filter, uiGridExporterService, uiGridExporterConstants, ReportApiService, $mdDateLocale) {
        $scope.datePicker = {};
        $scope.datePicker.date = { startDate: new Date(), endDate: new Date() };
        $scope.dateType = "E";
        $mdDateLocale.formatDate = function(date) {
            return moment(date).format('DD/MM/YYYY');
        };
        $scope.export = function(format) {
            var exportData = [];
            var exportColumnHeaders = $scope.gridOptions.showHeader ? uiGridExporterService.getColumnHeaders($scope.gridApi.grid, uiGridExporterConstants.VISIBLE) : [];
            angular.forEach($scope.gridApi.grid.rows, function(row) {
                if (row.visible) {
                    var values = [];
                    angular.forEach(exportColumnHeaders, function(column) {
                        var value = row.entity[column.name];
                        values.push({
                            value: value
                        });
                    });
                    exportData.push(values);
                }
            });
            var content;
            if (format == "csv") {
                content = uiGridExporterService.formatAsCsv(exportColumnHeaders, exportData, ',');
                uiGridExporterService.downloadFile($scope.gridOptions.exporterCsvFilename, content, $scope.gridOptions.exporterOlderExcelCompatibility);
            } else {
                content = uiGridExporterService.prepareAsPdf($scope.gridApi.grid, exportColumnHeaders, exportData);
                pdfMake.createPdf(content).download('sample.pdf');

            }
        };
        $scope.dateFormat = 'dd/MM/yyyy';

        $scope.fromDatePopup = {
            opened: false
        };
        $scope.endDatePopup = {
            opened: false
        };
        $scope.openFromDate = function() {
            $scope.fromDatePopup.opened = !$scope.fromDatePopup.opened;
        };
        $scope.openEndDate = function() {
            $scope.endDatePopup.opened = !$scope.endDatePopup.opened;
        };
        $scope.gridOptions = {
            enableGridMenu: true,
            exporterMenuCsv: false,
            exporterMenuPdf: false,
            gridMenuCustomItems: [{
                title: 'CSV Export (Filtered & Paged Grid)',
                action: function() {
                    $scope.export('csv');
                },
                order: 210
            }, {
                title: 'PDF Export (Filtered & Paged Grid)',
                action: function() {
                    $scope.export('pdf');
                },
                order: 250
            }],
            enableFiltering: true,
            paginationPageSizes: [15, 30, 45],
            paginationPageSize: 15,
            exporterCsvFilename: 'filteredData.csv',
            onRegisterApi: function(gridApi) {
                console.log("reachedddd");
                $scope.gridApi = gridApi;
                console.log("grid api", $scope.gridApi)
            },
            columnDefs: [
                { name: 'EntryDate', enableFiltering: false, type: 'date', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Name' },
                { field: 'PolicyNumber', displayName: "Policy#" },
                { name: 'SumAssured', enableFiltering: false },
                { name: 'PremiumAmount', enableFiltering: false },
                { name: 'ExpiryDate', enableFiltering: false, type: 'date', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Address', visible: false, enableFiltering: false },
                { name: 'PhoneNumber', enableFiltering: false },
                { field: 'MobileNumber1', displayName: "Mobile#" },
                { field: 'MobileNumber2', displayName: "Mobile# 1", visible: false },
                { field: 'MobileNumber3', displayName: "Mobile# 2", visible: false },
                { name: 'Email', visible: false }
                
            ],
            data: []
        };
        $scope.searchGenInsReport = function() {
            ReportApiService.searchGeneralInsReport($scope.datePicker.date, $scope.dateType)
                .then(function(response) {
                    console.log("response.....", response);
                    $scope.gridOptions.data = response.data;
                }, function(error) {
                    console.log("Error in search general inusrance", error);
                });
        };

    }
]);