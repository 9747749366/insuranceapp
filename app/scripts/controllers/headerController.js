'use strict';

var app = angular.module('testApp');
app.controller('HeaderController', ['$scope', '$location','$window',
    function($scope, $location,$window) {
        $scope.isActive = function(viewLocation){
            return viewLocation === $location.path();
        };
       
    }

]);