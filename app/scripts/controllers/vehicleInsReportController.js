'use strict';

var app = angular.module('testApp');
app.controller('VehicleInsReportController', ['$scope', 'InsuranceApiService', '$uibModal', '$filter', 'uiGridExporterService', 'uiGridExporterConstants', 'ReportApiService',
    function($scope, InsuranceApiService, $uibModal, $filter, uiGridExporterService, uiGridExporterConstants, ReportApiService) {
        $scope.datePicker = {};
        $scope.datePicker.date = { startDate: new Date(), endDate: new Date() };
        $scope.dateType = "E";


        $scope.dateFormat = 'dd/MM/yyyy';

        $scope.fromDatePopup = {
            opened: false
        };
        $scope.endDatePopup = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: 'false'
        };
        $scope.openFromDate = function() {
           
            $scope.fromDatePopup.opened = !$scope.fromDatePopup.opened;
        };
        $scope.openEndDate = function() {
            $scope.endDatePopup.opened = !$scope.endDatePopup.opened;
        };
        $scope.export = function(format) {
            var exportData = [];
            var exportColumnHeaders = $scope.gridOptions.showHeader ? uiGridExporterService.getColumnHeaders($scope.gridApi.grid, uiGridExporterConstants.VISIBLE) : [];
            angular.forEach($scope.gridApi.grid.rows, function(row) {
                if (row.visible) {
                    var values = [];
                    angular.forEach(exportColumnHeaders, function(column) {
                        var value = row.entity[column.name];
                        values.push({
                            value: value
                        });
                    });
                    exportData.push(values);
                }
            });
            var content;
            if (format == "csv") {
                content = uiGridExporterService.formatAsCsv(exportColumnHeaders, exportData, ',');
                uiGridExporterService.downloadFile($scope.gridOptions.exporterCsvFilename, content, $scope.gridOptions.exporterOlderExcelCompatibility);
            } else {
                content = uiGridExporterService.prepareAsPdf($scope.gridApi.grid, exportColumnHeaders, exportData);
                pdfMake.createPdf(content).download('sample.pdf');

            }
        }

        $scope.gridOptions = {
            enableGridMenu: true,
            exporterMenuCsv: false,
            exporterMenuPdf: false,
            exporterPdfDefaultStyle: { fontSize: 12 },
            exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, color: 'red' },
            exporterPdfHeader: { text: "Insurance Point", style: 'headerStyle' },
            exporterPdfFooter: function(currentPage, pageCount) {
                return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            },
            exporterPdfCustomFormatter: function(docDefinition) {
                docDefinition.styles.headerStyle = { fontSize: 22, bold: true, marginLeft: 250 };
                docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                return docDefinition;
            },
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 700,
            gridMenuCustomItems: [{
                title: 'CSV Export (Filtered & Paged Grid)',
                action: function() {
                    $scope.export('csv');
                },
                order: 210
            }, {
                title: 'PDF Export (Filtered & Paged Grid)',
                action: function() {
                    $scope.export('pdf');
                },
                order: 250
            }],
            enableFiltering: true,
            paginationPageSizes: [15, 30, 45],
            paginationPageSize: 15,
            exporterCsvFilename: 'filteredData.csv',
            onRegisterApi: function(gridApi) {
                console.log("reachedddd");
                $scope.gridApi = gridApi;
                console.log("grid api", $scope.gridApi)
            },
            columnDefs: [
                { name: 'EntryDate', enableFiltering: false, type: 'date', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { field: 'VehicleNumber', displayName: 'Vehicle#' },
                { field: 'RcOwner', displayName: 'RC Owner' },
                { name: 'PresentOwner' },
                { field: 'PolicyNumber', displayName: 'Policy#' },
                { name: 'ExpiryDate', enableFiltering: false, type: 'date', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Address', visible: false },
                { name: 'PhoneNumber', visible: false },
                { field: 'MobileNumber1', displayName: "Mobile#" },
                { name: 'MobileNumber2', visible: false },
                { name: 'MobileNumber3', visible: false },
                { name: 'Email', visible: false },
                { name: 'PremiumAmount', enableFiltering: false, cellClass: 'grid-align', cellFilter: 'number:2' }


            ],
            data: []
        };
        $scope.searchVehInsReport = function() {
            console.log($scope.datePicker.date)
            ReportApiService.searchVehicleInsReport($scope.datePicker.date, $scope.dateType)
                .then(function(response) {
                    console.log("response.....", response);
                    $scope.gridOptions.data = response.data;
                }, function(error) {
                    console.log("Error in search general inusrance", error);
                });
        };


    }
]);