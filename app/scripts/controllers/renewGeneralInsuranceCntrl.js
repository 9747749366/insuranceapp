app.controller('RenewGeneralInsuranceCntrl', function($scope, $uibModalInstance, $filter, generalInsObj, policyTypeList, InsuranceApiService) {
    $scope.policyTypeList = policyTypeList;
    $scope.generalInsuranceParams = {
        entryDate: new Date(),
        name: generalInsObj.Name,
        address: generalInsObj.Address,
        email: generalInsObj.Email,
        premiumAmount: generalInsObj.PremiumAmount,
        phoneNumber: generalInsObj.PhoneNumber,
        policyNumber: generalInsObj.PolicyNumber,
        sumAssured: generalInsObj.SumAssured,
        expiryDate: new Date().setFullYear(new Date().getFullYear() + 1),
        mobileNumber: [{ name: generalInsObj.MobileNumber1 }],
        policyType: generalInsObj.PolicyType
    };

    if (generalInsObj.MobileNumber2) {
        $scope.generalInsuranceParams.mobileNumber.push({ name: generalInsObj.MobileNumber2 })
    };
    if (generalInsObj.MobileNumber3) {
        $scope.generalInsuranceParams.mobileNumber.push({ name: generalInsObj.MobileNumber3 })
    };

    $scope.saveGeneralInsMessage = false;
    $scope.dateFormat = 'dd/MM/yyyy';

    $scope.expiryDatePopup = {
        opened: false
    };
    $scope.entryDatePopup = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };
    $scope.openExpiryDate = function() {
        $scope.expiryDatePopup.opened = !$scope.expiryDatePopup.opened;
    };
    $scope.openEntryDate = function() {
        $scope.entryDatePopup.opened = !$scope.entryDatePopup.opened;
    };
    $scope.addNewNumber = function(idx) {
        var numList = $scope.generalInsuranceParams.mobileNumber;
        if (numList[idx].name) {
            $scope.generalInsuranceParams.mobileNumber.push({ name: "" });
        }

    };

    $scope.showNumPlus = function(idx) {
        var numList = $scope.generalInsuranceParams.mobileNumber;
        if (numList.length - 1 == idx) {
            return true;
        }
    };

    $scope.saveGeneralInsDetails = function(form) {
        if (form.$valid) {
            var generalInsParams = $scope.generalInsuranceParams;
            InsuranceApiService.renewGenIns(generalInsParams, 1)
                .then(function(response) {
                    $scope.saveGeneralInsMessage = true;
                }, function(error) {
                    console.log("Error in saving general inusrance", error);
                });
        }

    };

    $scope.deleteNewNumber = function(idx) {
        $scope.generalInsuranceParams.mobileNumber.splice(idx, 1);
    };

    $scope.cancel = function() {
        $uibModalInstance.close('ok');
    };

});