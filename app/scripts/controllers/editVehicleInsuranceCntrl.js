app.controller('EditVehicleInsuranceCntrl', function($scope, $uibModalInstance, vehicleInsObj, makeList, InsuranceApiService, $filter) {

    $scope.makeList = makeList;
    $scope.vehicleinsuranceParams = {
        entryDate: new Date(vehicleInsObj.EntryDate),
        mobileNumber: [{ name: vehicleInsObj.MobileNumber1 }],
        rcOwner: vehicleInsObj.RcOwner,
        address: vehicleInsObj.Address,
        vehicleNumber: vehicleInsObj.VehicleNumber,
        policyNumber: vehicleInsObj.PolicyNumber,
        email: vehicleInsObj.Email,
        premiumAmount: vehicleInsObj.PremiumAmount,
        presentOwner: vehicleInsObj.PresentOwner,
        address: vehicleInsObj.Address,
        expiryDate:new Date(vehicleInsObj.ExpiryDate),
        make: vehicleInsObj.Make,
        phoneNumber: vehicleInsObj.PhoneNumber
    }


    if (vehicleInsObj.MobileNumber2) {
        $scope.vehicleinsuranceParams.mobileNumber.push({ name: vehicleInsObj.MobileNumber2 })
    };

    if (vehicleInsObj.MobileNumber3) {
        $scope.vehicleinsuranceParams.mobileNumber.push({ name: vehicleInsObj.MobileNumber3 })
    };
    $scope.dateFormat = 'dd/MM/yyyy';

    $scope.expiryDatePopup = {
        opened: false
    };
    $scope.entryDatePopup = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };
    $scope.openExpiryDate = function() {
        $scope.expiryDatePopup.opened = !$scope.expiryDatePopup.opened;
    };
    $scope.openEntryDate = function() {
        $scope.entryDatePopup.opened = !$scope.entryDatePopup.opened;
    };
    $scope.cancel = function() {
        $uibModalInstance.close('ok');
    };

    $scope.saveVehicleInsMessage = false;

    $scope.addNewNumber = function(idx) {
        var numList = $scope.vehicleinsuranceParams.mobileNumber;
        if (numList[idx].name) {
            $scope.vehicleinsuranceParams.mobileNumber.push({ name: "" });
        }

    };
    $scope.sameAsPresentOwner = function() {
        if ($scope.vehicleinsuranceParams.sameAs) {
            $scope.vehicleinsuranceParams.presentOwner = $scope.vehicleinsuranceParams.rcOwner
        } else {
            $scope.vehicleinsuranceParams.presentOwner = "";
        }
    };
    $scope.showNumPlus = function(idx) {
        var numList = $scope.vehicleinsuranceParams.mobileNumber;
        if (numList.length - 1 == idx) {
            return true;
        }
    };

    $scope.saveVehicleInsDetails = function(form) {
        console.log(form)
        if (form.$valid) {
            var vehicleInsParams = $scope.vehicleinsuranceParams;
            InsuranceApiService.editVehicleInsurance(vehicleInsParams, vehicleInsObj.VehicleInsuranceId)
                .then(function(response) {
                    $scope.saveVehicleInsMessage = true;
                }, function(error) {
                    console.log("Error in saving general inusrance", error);
                });
        }
    };

    $scope.deleteNewNumber = function(idx) {
        $scope.vehicleinsuranceParams.mobileNumber.splice(idx, 1);
    };

});