'use strict';



var app = angular.module('testApp');
app.controller('LoginController', ['$scope', 'LoginApiService', '$filter', '$window',
    function($scope, LoginApiService, $filter, $window) {
        $scope.loginCredentials = { username: "", password: "" };
        $scope.insLogin = function(form) {

            if (form.$valid) {
                LoginApiService.login($scope.loginCredentials)
                    .then(function(response) {
                        console.log("response...", response);
                    }, function(error) {
                        console.log("Error in search general inusrance", error);
                    });
            }
        };

        $scope.click = function(){
            $window.open('www.fabsv.in');
        } 
    }

]);