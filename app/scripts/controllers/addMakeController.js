'use strict';

var app = angular.module('testApp');
app.controller('AddMakeCntrl', ['$scope','$uibModalInstance','InsuranceApiService',
    function($scope,$uibModalInstance,InsuranceApiService) {
    	$scope.makeParams = {make:""};
        $scope.cancel = function() {
            $uibModalInstance.close('ok');
        };
        $scope.addNewMake = function(){
        	if($scope.makeParams.make){
                InsuranceApiService.addNewMake($scope.makeParams.make)
                    .then(function(response) {
                         $uibModalInstance.close('ok');
                    }, function(error) {
                        console.log("Error in saving general inusrance", error);
                    });
        	}
          
        };
    }

]);