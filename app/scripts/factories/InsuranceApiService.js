//This js file will contain all factory objects related to manage the metadata


/* Master metadata factory will store a persistant metadata model.
This model will be used to generate action for dropdowns and facets */
angular.module('testApp').factory('InsuranceApiService', ['$http', '$q', 'config', '$filter', function($http, $q, config, $filter) {

    var insApiService = {};

    insApiService.saveGeneralInsuranceDetails = function(params, renew) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        delete params.policyType.$$hashKey;
        var generalInsObj = {
            Address: params.address,
            Email: params.email,
            Name: params.name,
            EntryDate: $filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            PremiumAmount: params.premiumAmount,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            Renew: renew,
            PolicyType: params.policyType.PolicyTypeId
        };


        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/GeneralInsurance',
            data: generalInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;
    };
     insApiService.renewGenIns = function(params, renew) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        delete params.policyType.$$hashKey;
        var generalInsObj = {
            Address: params.address,
            Email: params.email,
            Name: params.name,
            EntryDate: $filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            PremiumAmount: params.premiumAmount,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            Renew: renew,
            PolicyType: params.policyType
        };


        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/GeneralInsurance',
            data: generalInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;
    };

    insApiService.editGeneralInsurance = function(params, id) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        var generalInsObj = {
            Address: params.address,
            Email: params.email,
            Name: params.name,
            EntryDate:$filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            PremiumAmount: params.premiumAmount,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            GeneralInsuranceId: id,
            PolicyType: params.policyType
        };

        var deferred = $q.defer();
        $http({
            method: 'PUT',
            url: config.url + '/api/GeneralInsurance',
            data: generalInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;

    };

    insApiService.searchGeneralIns = function(params) {
        var mobNum = 0;
        if (params.MobileNumber) {
            mobNum = params.MobileNumber;
        }
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/GeneralInsurance?MobileNumber=' + mobNum + '&PolicyNumber=' + params.PolicyNumber
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };

    insApiService.saveVehicleInsurance = function(params, renew) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        delete params.make.$$hashKey;
        delete params.make.$$mdSelectId;
        var vehicleInsObj = {
            Address: params.address,
            Make: params.make,
            Name: params.name,
            EntryDate: $filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            RcOwner: params.rcOwner,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            Renew: renew,
            PresentOwner: params.presentOwner,
            VehicleNumber: params.vehicleNumber,
            PremiumAmount: params.premiumAmount,
            Email: params.email
        };

        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/VehicleInsurance',
            data: vehicleInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;

    };
    insApiService.renewVehicleInsurance = function(params, renew) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        delete params.make.$$hashKey;
        var generalInsObj = {
            Address: params.address,
            Make: params.make,
            Name: params.name,
            EntryDate: $filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            RcOwner: params.rcOwner,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            Renew: renew,
            PresentOwner: params.presentOwner,
            VehicleNumber: params.vehicleNumber,
            PremiumAmount: params.premiumAmount,
            Email: params.email
        };

        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/VehicleInsurance',
            data: generalInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;

    }

    insApiService.searchVehicleIns = function(params) {
        var mobNum = 0;
        if (params.mobileNumber) {
            mobNum = params.mobileNumber;
        }
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/VehicleInsurance?MobileNumber=' + mobNum + '&PolicyNumber=' + params.policyNumber + '&VehicleNumber=' + params.vehicleNumber
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };


    insApiService.editVehicleInsurance = function(params, id) {
        var number = params.mobileNumber;
        var mobileNumber1 = "";
        var mobileNumber2 = "";
        var mobileNumber0 = number[0].name;
        if (number.length >= 2) {
            mobileNumber1 = number[1].name;
        }
        if (number.length >= 3) {
            mobileNumber2 = number[2].name;
        }
        var vehicleInsObj = {
            Address: params.address,
            Make: params.make,
            Name: params.name,
            EntryDate: $filter('date')(params.entryDate, "dd/MM/yyyy"),
            PhoneNumber: params.phoneNumber,
            PolicyNumber: params.policyNumber,
            ExpiryDate: $filter('date')(params.expiryDate, "dd/MM/yyyy"),
            SumAssured: params.sumAssured,
            RcOwner: params.rcOwner,
            MobileNumber1: parseInt(mobileNumber0),
            MobileNumber2: parseInt(mobileNumber1) ? parseInt(mobileNumber1) : 0,
            MobileNumber3: parseInt(mobileNumber2) ? parseInt(mobileNumber2) : 0,
            VehicleInsuranceId: id,
            PolicyTypeId: 1,
            PresentOwner: params.presentOwner,
            VehicleNumber: params.vehicleNumber,
            PremiumAmount: params.premiumAmount,
            Email: params.email
        };

        var deferred = $q.defer();
        $http({
            method: 'PUT',
            url: config.url + '/api/VehicleInsurance',
            data: vehicleInsObj,
        }).then(function(data) {
            deferred.resolve(data);
        }), (function(err) {
            deferred.reject(err)
        });

        return deferred.promise;

    };

    insApiService.getMake = function() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/VehicleMake'
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };

    insApiService.getPolicyType = function() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/PolicyType'
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };

    insApiService.addNewMake = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/VehicleMake',
            data: { Id: 1, Name: params },
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };

    insApiService.addPolicyTYpe = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.url + '/api/PolicyType',
            data: { Id: 1, Name: params },
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;
    };
    return insApiService;


}]);