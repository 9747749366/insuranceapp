angular.module('testApp').factory('LoginApiService', ['$http', '$q', 'config', function($http, $q, config) {

    var loginApiService = {};
    loginApiService.login = function(params) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/Employee?LoginName=' + params.username + '&Password=' + params.password 
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;

    };
    

    return loginApiService;


}]);