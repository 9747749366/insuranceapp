
angular.module('testApp').factory('ReportApiService', ['$http', '$q', 'config', '$filter', function($http, $q, config, $filter) {

    var reportApiService = {};
    reportApiService.searchGeneralInsReport = function(date, dateType) {
        console.log("date...",date)
        var fromDate = $filter('date')(date.startDate, "dd/MM/yyyy");
        var todate = $filter('date')(date.endDate, "dd/MM/yyyy");
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/GeneralInsurance?FromDate=' + fromDate + '&ToDate=' + todate + '&DateType=' + dateType
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;

    };
    
    reportApiService.searchVehicleInsReport = function(date, dateType) {
        var fromDate =  $filter('date')(date.startDate, "dd/MM/yyyy");
        var todate = $filter('date')(date.endDate, "dd/MM/yyyy");
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.url + '/api/VehicleInsurance?FromDate=' + fromDate + '&ToDate=' + todate + '&DateType=' + dateType
        }).then(function(data) {
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error)

        });
        return deferred.promise;

    };

    return reportApiService;


}]);