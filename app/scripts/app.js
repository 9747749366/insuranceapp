'use strict';
angular.module('testApp.usermanagement', []);
angular.module('testApp.modules', ['testApp.usermanagement']);
angular.module('testApp.grid', ['ui.grid', 'ui.grid.edit', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.resizeColumns', 'ui.grid.selection', 'ui.grid.exporter', 'ui.grid.autoResize', 'ui.grid.moveColumns', 'ui.grid.saveState', 'ui.grid.infiniteScroll']);
angular.module('testApp', ['ngAnimate', 'ngRoute', 'ngMaterial', 'ui.bootstrap', 'daterangepicker', 'testApp.grid', 'testApp.modules','720kb.datepicker'])
    .config(function($routeProvider) {
        $routeProvider
            .when('/general-insurance', {
                controller: 'GeneralInsuranceCntrl',
                templateUrl: 'views/generalInsurance.html'
              /*  resolve: {
                    authorize: ["UserManagement", "$location", function(UserManagement, $location) {
                        return UserManagement.authenticateUser()
                            .then(function(userInfo) {
                                if (!userInfo) {
                                    $location.path("#/login");
                                  
                                }
                                
                            })
                            .catch(function(error){
                                 $location.path("/login");
                            })
                    }]
                }
*/

            })
            .when('/', {
                templateUrl: 'views/vehicleInsurance.html',
                controller: 'VehicleInsuranceCntrl'
            })

            .when('/gen-ins-report', {
                templateUrl: 'views/generalInsReport.html',
                controller: 'GeneralInsReportController'
            })

            .when('/veh-ins-report', {
                templateUrl: 'views/vehicleInsReport.html',
                controller: 'VehicleInsReportController'
            })

            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginController'
            })

            .when('/masters', {
                templateUrl: 'views/master.html',
                controller: 'MasterController'
            })

            .otherwise({
                redirectTo: '/'
            });
    });
