var app = angular.module('testApp');
app.config(function(datepickerConfig, datepickerPopupConfig) {
    datepickerConfig.showWeeks = false;
    datepickerPopupConfig.toggleWeeksText = null;
});